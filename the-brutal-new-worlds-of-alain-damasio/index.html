<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="/favicon.ico" />
    <title>The Brutal New Worlds of Alain Damasio | Digressional Didact</title>
    <meta name="description" content="The French author who sees the future. Spoiler: it isn&#39;t awesome." />
    

    <link rel="stylesheet" href="https://cdn.simplecss.org/simple.min.css">
    <link rel="stylesheet" href="/css/admonitions.css">
</head>
<body>
    <header>
  <h1>The Brutal New Worlds of Alain Damasio</h1>
  <p>The French author who sees the future. Spoiler: it isn&#39;t awesome.</p>
  <nav>
  <a href="/" title="">Home</a><a href="/blog" title="">Blog</a><a href="/tags" title="">Tags</a><a href="/about" title="">About</a><a href="/contact" title="">Contact</a><a href="/blogroll" title="">Blogroll</a>
</nav>
</header>
<main>
<nav align="center">
    
        
            <a href="https://bbbhltz.codeberg.page/tags/books/">#books</a>
            <a href="https://bbbhltz.codeberg.page/tags/sci-fi/">#sci-fi</a></nav>


<p>
<b>Posted</b>: 13 November 2021
<br><b>Stats</b>: 3869 words / ~19  minutes </p>

<p>Dystopia in literature is common. The worlds of Huxley and Orwell, when first described, seemed so far away. Today we read them and find ourselves surprised, shocked and even disgusted by how similar these fictions are to our reality. Companies with immense power; governance and leadership leveraging technology in unethical manners; and our social lives falling apart are common themes in many dystopic fictions.</p>
<p>Alain Damasio is a French author. While he may not be known throughout the world, he clearly falls into the same category as our dear Huxley and Orwell. Over the course of 20 years, he has published three novels, two of which we will look at. Not wanting to spoil anything in the event that you decide to procure a copy, or find a translation, I will do my best to keep the details of the actual stories limited and focus on specific details.</p>
<p><em>La Zone du Dehors</em> was published in 1999, and <em>Les Furtifs</em> in 2019. The former is a story that takes place on a moon, the latter in France. Together, they describe things that have come to pass and things that have yet to pass. We can therefore look at these works as a diptych in which the themes of totalitarianism, social-democracy, mass surveillance, social hierarchy, freedom, multinational companies, technology and control (to name a few) play a major part of the story.</p>
<h2 id="things-that-have-come-to-pass">Things that have come to pass</h2>
<p>
  <figure>
    <img src="../images/cover_zone.jpg" alt="Cover: La Zone du Dehors">
    <figcaption>La Zone du Dehors</figcaption>
  </figure>

</p>
<p><em>La Zone du Dehors</em> was written in 1999, before the arrival of social networks, before society began worrying deeply about mass surveillance, and certainly before terms like &ldquo;social credit&rdquo; became part of our working vocabulary. This work of fiction can be considered rather prescient today for different reasons. The major theme throughout is tracking and tracing: all citizens are tracked. Their actions, their interactions, their associations are all tracked and can be used against them.</p>
<h3 id="creating-a-social-hierarchy">Creating a Social Hierarchy</h3>
<p>Much like in Huxley&rsquo;s <em>Brave New World</em> and Orwell&rsquo;s <em>1984</em>, there is a social hierarchy in place in Damasio&rsquo;s <em>Zone</em> (a place called Cerclon). This hierarchy not only decides how you circulate and where you can go, but it also decides your name. The &ldquo;president&rdquo; is, of course, named &ldquo;A&rdquo; and the world is controlled by this person and the other single-letters. The two-letters have a little less power, and it goes down to the 5-letters. The protagonist is &ldquo;Captp&rdquo; and he associates with other 5-letters like &ldquo;Kamio&rdquo; and &ldquo;Bdcht&rdquo;.</p>
<p>Your name is therefore your social ranking.</p>
<p>It wouldn&rsquo;t be fair, however, if you were born a 5-letter and had no chance of social mobility, would it? So, there is a system in place. A voting system. The citizens rank each other. There are votes or elections that take place and your name, which is your rank, is corrected (Damasio uses the portmanteau of <em>class</em> and <em>caste</em> for this vote: the <em>Clastre</em>). You might move up, or down. It is others who decide. Like something from <em>Black Mirror</em>, only in real life. Like being an influencer, but being treated as special in real life because random people decided you were special.</p>
<p>Obviously, there are similarities here to a certain social credit system we can see in the east. I have not been to that particular country, so I won&rsquo;t attempt to draw parallels. We can all agree, though, that a system that rewards someone for reporting a social faux-pas does exist today, and in that system your circulation can be limited according to your social credit. It is also the basis for our social networks, which many of us consider part of ourselves. Reddit has upvoting and downvoting and a karma system, and Instagram and Facebook use different algorithms to decide who sees what first. This virtual karma-point system has spilled over into real life, where an action, whether recent or not, can result in &ldquo;cancellation&rdquo; and job loss when the hordes of the Internet and mass media decide to wage war, and the companies that employ those individuals fear loss of profit.</p>
<p>This concept has become a reality since publication. While the majority of people would never let a system like this dictate who they can be friends with (good for them!) there are some people who do. Since 1999 we have been witness to hate against groups and minorities. Thanks to (and I say that positively) the rise in access to the Internet, we have been made aware of this hate that we once thought rare, or something our grandparents&rsquo; generation had done away with. We can also look at the younger generation and how they treat each other in public based on style or the brand of smartphone they own (look it up, there are kids bullied at school because they have Android instead of Apple).</p>
<p>(Another real example is the recent arrival of <a href="https://en.wikipedia.org/wiki/Immunity_passport#EU_Digital_COVID_Certificate">immunity passports</a> for COVID-10. In Damasio&rsquo;s world, the concept is put forward as a method for controlling circulation: certain citizens have access to certain places that others do not. There is a slight difference with Damasio&rsquo;s idea, but the parallel remains: there is a group that <em>can</em> and another that <em>cannot</em>. I will come back to this idea below.)</p>
<p>Finally, on a larger scale, gentrification has forced some families to live in areas with access to less adequate education and therefore less access to higher paying jobs. Social mobility has been reduced in such a way that it took a pandemic to highlight this phenomenon in a clear and understandable way: think of the students who were unable to attend online classes because they did not have access to Internet or a computer in their homes.</p>
<h3 id="the-place-of-technology">The Place of Technology</h3>
<p>Glorious technology! Praise be to its creators!</p>
<ul>
<li><strong>Steve Jobs</strong>, a college dropout that lied to his best friend, never programmed anything, and did not know how to build a computer (correct me if I am wrong) is seen as our saviour. Besmirching his name is a sin, blessed be his name, and all hail the iPhone.</li>
<li><strong>Mark Zuckerberg</strong>, who studied computer science and psychology, created a website to rank students on their appearance and grew into the largest social network, Facebook. This network collects and sells data (through brokers) that can be used by companies and governments to leverage power, and also spread lies. He is power, he is fear. He sat in front of congress and was treated as a peer. He is untouchable.</li>
<li><strong>Jeff Bezos</strong>, the richest person in the world (depending on the day of the week), worked in fintech and a hedge fund before borrowing money from his parents and banks to start a company that did not see profit for years. He pays his workers low wages and expects them to never take a break. Amazon kills local businesses and hurts the planet. He &ldquo;got big fast&rdquo; by investing in infrastructure, some of which is used today as a service by other companies. Beware Bezos, his blood runs cold.</li>
<li><strong>Elon Musk</strong> is not really a self-made man, and did not really found Tesla Motors. Our image of him is the new cool guy that smokes blunts, but he has been on the scene since the 1990s. Is he a genius? Is it all marketing? Is he just in it for the money? Approach with caution; do not follow on Twitter.</li>
</ul>
<p>In the time since <em>La Zone du Dehors</em> was published, technology companies have become the new oil. A successful piece of technology or software can launch a company or individual to newfound glory and privilege. Amazon&rsquo;s Alexa is a key example, as is Instagram.</p>
<p>In this story, many citizens have a small black, voice-activated, box in their homes. Sometimes it doesn&rsquo;t understand what you say, requiring you to yell at it (sounds credible today, doesn&rsquo;t it?) in order to turn on the television, make a phone call, or read the news. The generalization of voice commands, to paraphrase the author, is both &ldquo;magical&rdquo; and &ldquo;insidious&rdquo;:</p>
<blockquote>
<p>It had taken away the once common relationship with objects, the hand-to-hand contact, however stupid, with the machine, the buttons, the keyboard that we hammered away on&hellip; We didn&rsquo;t touch anything any more: we spoke, and the world activated. As such, we all saw ourselves as gods &mdash; or as police [&hellip;] The worst is that we developed a taste for it[.] [&hellip;] It had become a common habit: command without resistance&hellip;</p>
</blockquote>
<p>Another gadget found in many homes is the amazing <em>Mirécran</em>, or Mirror-screen. Today, we have all heard at least one time the term &ldquo;body dysmorphia&rdquo;. In 1999, maybe not as much. Damasio foresaw something. Here is the elevator pitch for this remarkable device:</p>
<blockquote>
<p>You have a mirror, the most normal of things. In fact, it is a vertical screen with a series of cameras behind it. When you look in the mirror, the cameras turn on and record your face from various angles. The recordings are filtered and retouched by a computer &mdash; often erasing wrinkles, highlighting eye colour or lip contours&hellip; The image is retouched and displayed on the screen. This happens in real time, to give you the impression that it is your face in the reflection&hellip;when in fact it is a video. People buy this, you know! Have a crooked nose, pimples, wrinkles? Not a problem with the Mirrorscreen. &ldquo;Mirrorscreen, the mirror that lies&rdquo;. You laugh, but that is their slogan! I&rsquo;m not making any of this up! Risky, isn&rsquo;t it? And it works, very well I might add, people love it: <strong>they see themselves exactly how they want to be seen</strong>.</p>
</blockquote>
<p>How is that not a description of Instagram, Snapchat and any other app that lets you filter and modify your appearance? That would have seemed cute and innocuous in 1999, but now we know what happens when people have access to this technology: they try to get plastic surgery to look like their fictional selves, and the celebrities sue to have unflattering images removed from circulation.</p>
<p><em>La Zone du Dehors</em> also gave us a template for the connected shopping experience, where your eyes are tracked and everything is automatically charged to your account. Shoppers do not look at each other or speak because they are engrossed in their connected experience. While this hasn&rsquo;t happened as described in the book, the other was right about many things. Eye tracking has been used and shopping has been optimized in ways that we are unaware of. We are not going to get to a point where we wear special glasses for shopping, or a little robot arm to pick and choose for us, but we are at a point where we never take off our headphones and keep one hand free for our phone &mdash; this has removed any social aspect from shopping. It has also made the gesture of taking off/out your headphones at the cash the modern day equivalent of tipping your hat to be polite. We also have this now:</p>
<div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden;">

  <iframe src="https://yewtu.be/embed/IMPbKVb8y8s" style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; border:0;" allowfullscreen title="Inside Amazon&#39;s Smart Warehouse">
  </iframe>

  <figcaption>Inside Amazon&#39;s Smart Warehouse</figcaption>

</div>

<p>We know that devices like Alexa, that record us non-stop, and software like Instagram, which can lead to serious social and behavioural problems, are negative. We also are aware that this is a much larger concern with technology: it can affect entire populations.</p>
<h3 id="leveraging-technology-for-power">Leveraging Technology for Power</h3>
<p>&ldquo;Information Economy&rdquo; is not a new term, but it was still closer to fiction in 1999 than it is today. In the world of <em>La Zone du Dehors</em> freedom and power are controlled through technology.</p>
<blockquote>
<p>&ldquo;[&hellip;] I&rsquo;m not talking about controlling ideas, gentle propaganda or the idealogical models that we maintain &mdash; what government doesn&rsquo;t do that? I&rsquo;m talking about a more subtle and powerful control, one that doesn&rsquo;t simply cover you from the outside like a straightjacket, but one that comes from you, the root, to purify it. An internal control, intimate, <em>in petto</em>, that acts on the primary emotional centres: fear, aggression, desire, love, pleasure, unease&hellip;&rdquo;</p>
</blockquote>
<p>This is the play-book for politicians today. Use social networks to control citizens. Not directly, but emotionally. The information that advertisers and politicians can have access to from Facebook, to name one, has been used to <em>&ldquo;control the affective components of a motive to action&rdquo;</em>. Marketers use techniques that governments have since used to achieve what one character in the book calls the &ldquo;peak of power&rdquo;: <em><strong>An optimum alienation under the appearance of total freedom.</strong></em></p>
<p>Television is a notable tool in this undertaking, described in the book as something that &ldquo;conforms more than it informs&rdquo; allowing them to control us. Them? Who are they? We always say, &ldquo;they control us,&rdquo; and this story gives us a possible answer:</p>
<blockquote>
<p>&ldquo;Who are they?&rdquo; <br>
&ldquo;You.&rdquo; <br>
&ldquo;Me?&rdquo; <br>
&ldquo;You all, me, us. All citizens.&rdquo; <br>
[&hellip;] <br>
&ldquo;Because some people or groups, due to their strategic position in the social network, boost this control more easily than others. They also suffer from it.&rdquo;</p>
</blockquote>
<p>Damasio did not foresee the social networks that we currently have. He imagined television personalities as having that influencer power. Oprah-level power.</p>
<p>Damasio&rsquo;s <em>La Zone du Dehors</em> sounds like a call to action when read today. We are getting to the point where more people are asking questions about their private information because we learned the hard way what answering a quiz on Facebook could lead to.</p>
<p>Technology never stops. Things improve, accelerate, ameliorate. Since 1999, a large chunk of the world has gone from no Internet or low-bandwidth Internet, to high-speed Internet, to a wireless connected world. We now willingly carry tracking devices in our pockets. We share health data with companies, so they can feed us ads. There are numerous wireless protocols, drones, cameras, listening devices and other connected gadgets deployed around our towns and cities. What&rsquo;s next?</p>
<h2 id="things-that-have-yet-to-pass">Things that have yet to pass</h2>
<p>
  <figure>
    <img src="../images/cover_furtifs.jpg" alt="Cover: Les Furtifs">
    <figcaption>Les Furtifs</figcaption>
  </figure>

</p>
<p>20 years after <em>La Zone du Dehors</em>, Damasio published <em>Les Furtifs</em> (&ldquo;Stealthies&rdquo;, in English). While not as blatantly tech-focused as the former, this latter work carries the same message: Fighting against tracking is a worthy fight. This message is expressed through a story that takes place in France, rather than on a moon, just 20 years or so in the future. The themes of the previous work are magnified, with an emphasis added to the role of multinational corporations.</p>
<h3 id="the-role-of-mncs">The Role of MNCs</h3>
<p>Damasio&rsquo;s vision of France in the future is one where entire cities belong to companies. The city of Orange belongs, accordingly, to Orange (a telecoms company). Paris belongs to LVMH, Cannes to <em>Dysflix</em> (Disney+Netflix). Outside of France, the same phenomenon: Brussels belongs to Alphabet, and is known as <em>AlphaBrux</em>. The role of the Multinational is that of control. Much like in Huxley&rsquo;s vision, the MNC has &ldquo;freed&rdquo; the population by controlling them.</p>
<blockquote>
<p>[Orange] was born the 7<sup>th</sup> of December 2021, when 70 protesters from the <em>Take Back</em> collective were crushed under 200 tonnes of rubble. And the 29 families who still lived in the tower, whom they were defending. It was born from the bankruptcy of a city strangled by banks, downgraded to a triple C by the international ranking agencies, and forced by borrow its budget at 18% interest rates; of a city declared in arrears in 2028, abandoned by the State and auctioned off in 2030 on the free city market.</p>
</blockquote>
<p>Freed them from what? Bankruptcy. Bank loans. High interest rates. Take your pick. The cities were sold to the highest bidders. A &ldquo;Free City&rdquo; is free from the State, but wholly controlled by the company that purchased it. The mayor is chosen by shareholders. The cities then privatize services, and offer subscriptions in order to gain access. Certain streets are reserved for those who can afford.</p>
<p>The privatized cities are filled with AI-controlled taxis, drones keeping tabs on you, and panhandlers known as &ldquo;vendiants&rdquo;, which is a French portmanteau of &ldquo;vendeur&rdquo; (seller) and &ldquo;mendiant&rdquo; (beggar) which I will call &ldquo;sellhandlers&rdquo;.</p>
<blockquote>
<p>The sellhandlers panhandle their sales. They don&rsquo;t even beg for themselves, like our old beggars: they beg for their brand, their product, their masters, for a platform floating in the cloud where they will never meet a single manager or see, except maybe on a phone, the start of a head of sales.</p>
</blockquote>
<p>Most citizens wear a ring, which contains their profile, allowing the sensors to know their preferences. Without the ring, you will be solicited non-stop. Progress doesn&rsquo;t stop, however, and a newer and better&mdash;and more expensive&mdash;ring is always around the corner.</p>
<blockquote>
<p>As soon as you go out in the street, in any privatized city, you are systematically hit by three waves: <em>taxiles</em>, <em>sellhandlers</em>, and drones. [&hellip;] <br>
Without a ring, you have no ID according to the sensors, the network. No profile, no preferences, no possible personalization with regard to solicitation or being left alone.</p>
</blockquote>
<h3 id="the-technococoon-and-controlling-citizens">The &ldquo;Technococoon&rdquo; and Controlling Citizens</h3>
<p>Politicians maintain control through scrupulous and calibrated techniques. None of them are novel, but rather enhanced by technology. Different segments; different words; different promises. <em>Affecting</em> and <em>fiction scripting</em>, provoking emotional responses and controlling the narrative, are leveraged to the point of being a weapon. As such, it is possible to manipulate the population into choosing the next leaders.</p>
<p>Such a feat would not be possible without mass surveillance, and tracking.</p>
<blockquote>
<p>Without tracking, control is not possible [&hellip;]. You know it. No sustainable security. And without tracking, our algorithms cannot personalize your experience in the city. <strong>How can we give you the city you deserve if we don&rsquo;t know what you like?</strong> If we don&rsquo;t know about your habits, your preferred routes, your tastes?</p>
</blockquote>
<p>And so, the citizens of these cities wear rings and use apps&ndash;like <em>MOA</em> (My Own Assistant). The publishers behind <em>Les Furtifs</em>, <em>La Volte</em>, even took the time to show us what this is like (in French, albeit):</p>
<div class='embed-container' align='center'>
<iframe width="800" height="450" title="MOA - My Own Assistant" src="https://video.lavolte.net/videos/embed/556ac85e-a7d1-4bb6-99d3-d8a4d0afc48a" frameborder="0" allowfullscreen></iframe>
<figcaption>MOA - My Own Assistant</figcaption>
</div>
<p>Chilling, if it were not already underway today with our current selection of social networking applications.</p>
<p>The choice is yours, however, to wear the ring and use the application. Your choice, until it isn&rsquo;t any longer. You can be forced to wear the ring as punishment if you commit a crime, for example.</p>
<p>The citizens of this speculative future live in an &ldquo;electric smog,&rdquo; or &ldquo;technococoon,&rdquo; where they remain a <em>chrysalis</em>, and never become a &ldquo;butterfly&rdquo;. They keep themselves there, living their lives in a second, &ldquo;ultimate&rdquo;, reality where they feel they are the masters.</p>
<p>In the <em>ultimate reality</em>, an individual can choose the people and animals they see. The first reality, the banal one, cannot be shared like the <em>ultimate reality</em>. The second reality can be individualized to the point of being a product. Gadgets and applications were just the foot in the door, the <em>ultimate reality</em>, a sort of <em>metaverse</em>, was the real invasion.</p>
<h3 id="loss-of-freedom-and-social-decay">Loss of Freedom and Social Decay</h3>
<p>Saying the words, &ldquo;technology studying behaviour,&rdquo; however common it already is, sounds creepy. Saying, &ldquo;technology knowing your tastes, desires, and anticipating needs,&rdquo; is also creepy, but can at least be useful. They are one and the same, obviously, and not in any way synonymous with &ldquo;freedom.&rdquo;</p>
<p>It is easy to imagine a world where the majority of the population is, in one way or another, addicted to something like a phone or a social network. When Apple produces a new device, or microfiber cleaning rag, people line up like automatons getting a recharge. When I go to the park, many of the parents are on their phones. I would be too, if I had a smartphone, and if my son wasn&rsquo;t keeping me on my toes.</p>
<p>Let&rsquo;s bump it up a notch and give our kids addictive gadgets too. Like a virtual Pokémon or Tamagotchi that they can see, and let others see, through connected contact lenses.</p>
<blockquote>
<p>The ring, it&rsquo;s like a hand. Losing it, that&rsquo;s an amputation! <br>
[&hellip;] <br>
[The children] take their virtual toy everywhere, they see it everywhere, in the school yard, on the bus, in the cafeteria&hellip;</p>
</blockquote>
<p>Some parents in <em>Les Furtifs</em> go as far as naming their children after a company. Why? Royalties.</p>
<p>Huxley&rsquo;s <em>Brave New World</em> talked about this possible future as well. A future where we are given complicated, addictive, entertainment machines. The machines don&rsquo;t come cheap. You need to work for them, and the update, and the upgrade, and the next disruptive gadget. Technology keeps people working.</p>
<p>Between working to pay for new things, and the time spent using new things, we will lose something: the art of the encounter. Meeting new people. Socializing. Damasio&rsquo;s future is one where socializing is decaying, and he is opposed to that.</p>
<p>The need to socialize is not gone in his story, though. There are even virtual cabbies that chat to you about subjects of interest. Socializing, meeting, encountering, discovering new things, is the last thing keeping us alive.</p>
<h2 id="conclusion">Conclusion</h2>
<p>I&rsquo;ve been working on this little write-up for a few months. A sentence here. A correction there. Over the past few weeks I&rsquo;ve found myself motivated to complete it, but also distracted, by <a href="https://arstechnica.com/gaming/2021/11/everyone-pitching-the-metaverse-has-a-different-idea-of-what-it-is/">the drama surrounding Facebook</a>. The concept of a metaverse has been visited many times in science-fiction, most notably by Neal Stephenson in <em>Snow Crash</em> (1992), and is soon to be a real thing. Seoul announced they will soon &ldquo;<a href="https://qz.com/2086353/seoul-is-developing-a-metaverse-government-platform/">join the metaverse</a>&rdquo;:</p>
<blockquote>
<p>&ldquo;Seoul’s metropolitan government will develop its own metaverse platform by the end of 2022. By the time it is fully operational in 2026, it will host a variety of public functions including a virtual mayor’s office, as well as spaces serving the business sector; a fintech incubator; and a public investment organization. <br>
[&hellip;] <br>
The future of the metaverse is being built almost entirely by companies. Microsoft, Nike, and Facebook’s parent company Meta are all staking claims to digital real estate. South Korea is among the only governments attempting to recreate the virtual public square. But if they can, it could expand the utility of the metaverse to millions of citizens whom might otherwise be excluded.&rdquo;</p>
</blockquote>
<p>Some of what Damasio envisioned in <em>La Zone du Dehors</em> has come to pass, and now it seems that the future imagined in <em>Les Furtifs</em> is on our doorstep. I am at odds. The cynical angel on one shoulder has thrown in the towel, stating that &ldquo;we are all screwed&rdquo; between the companies taking over, and the climate crisis. The optimistic angel, on the other shoulder, has his fingers crossed, hoping that we will snap out of it.</p>
<p>The optimist, much like <a href="https://www.newyorker.com/culture/office-space/the-question-weve-stopped-asking-about-teen-agers-and-social-media?utm_brand=the-new-yorker&amp;utm_social-type=earned">Cal Newport argues in The New Yorker</a>, and <a href="https://pvto.weebly.com/uploads/9/1/5/0/91508780/eight_o%E2%80%99clock_in_the_morning-nelson.pdf">Ray Nelson in &ldquo;Eight O&rsquo;Clock in the Morning,&rdquo;</a> hopes that we will see things clearly and ask the right questions. There is another way. We do not need to give ourselves, and <a href="https://www.bit-tech.net/news/gaming/pc/gamestation-we-own-your-soul/1/">our souls, to companies</a>. We are allowed, on an individual and collective level, to say <em>no</em> and call <em>bullshit</em> when people in power begin to use powerful, complicated, technology to do unethical things.</p>
<p>A utopia isn&rsquo;t the answer, or a possibility for the moment, but let&rsquo;s not aim for a dystopia.</p>
<hr>
<p>These books are published by <strong>La Volte</strong>, an independent publisher.</p>
<p>As far as I can tell, they have not been published in other languages. If you want to read them in a language other than French, you may have to wait.</p>
<p>You can find more information, in French, on Alain Damasio on <a href="https://lavolte.net/auteurs/alain-damasio">lavolte.net</a>. You can also follow <strong>La Volte</strong> on <a href="https://imaginair.es/@lavolte">Mastodon</a>, <a href="https://t.me/lavolte">Telegram</a> and <a href="https://video.lavolte.net/accounts/lavolte/videos">Peertube</a>.</p>


</main>
    <footer>
<a href="/rss.xml">RSS</a> / <a rel="me" href="https://fosstodon.org/@bbbhltz">Fosstodon</a> / <a href="https://codeberg.org/bbbhltz/pages">Source</a><br>

<a href="https://creativecommons.org/licenses/by-sa/4.0/">CC BY-SA 4.0</a><br>

<a href="https://512kb.club"><img src="https://512kb.club/assets/images/green-team.svg" style="height:1.5em;"></a><br>

Surf the <a href="https://fediring.net/">Fediring</a>: <a href="https://fediring.net/previous?host=bbbhltz.codeberg.page">Previous</a> /  <a href="https://fediring.net/next?host=bbbhltz.codeberg.page">Next</a><p>

<a href="https://codeberg.org/bbbhltz" rel="me"></a>
<a href="https://framagit.org/bbbhltz" rel="me"></a>
<a href="https://github.com/pasdechance" rel="me"></a>

</footer>
</body>
</html>
