---
title: Transformers
subtitle: Robots in Disguise
date: 2021
institute: "bbbhltz school of nonsense"
subject: Transformers
author: "[bbbhltz](mailto:bbbhltz@mailbox.org)"
titlegraphic: Transformerslogo.png
lang: en-GB
toc: yes
dpi: 300
colortheme: whale
outertheme: infolines
innertheme: rectangles
mainfont: FreeSans
monofont: FreeMono
usepackage: xeCJK
CJKmainfont: Noto Sans CJK SC Regular
---

# Introduction

## History

+-----------+-------------+
| Autobots  | Decepticons |
+===========+=============+
| Wheeljack | Ravage      |
+-----------+-------------+

# Autobots

## Wheeljack

![Wheeljack WFC: Earthrise](https://s3.amazonaws.com/tf.images/image_17499_0_1582059099.jpeg){ height=50% }

### Characteristics

* smart
* cool
* badass

# Decepticons

## Ravage

![Video by Chris McFeely](ytravage.png){ height=50% }

[↪ Watch video on YouTube](https://www.youtube.com/watch?v=bONtMlzdgus)

# Tricky Bits

## Chinese Characters

Example: 可口可乐

## Animations

yadda \pause

yadda \pause

yadda

## Animations Bottom-to-Top

\uncover<3->{yadda}

\uncover<2->{yadda}

\uncover<1->{yadda}

## Adding some colour

\alert{ALTERTED YADDA}

\textcolor{violet}{VIOLET YADDA}

## Text Size

:::: columns
::: column

\tiny{yadda}

\scriptsize{yadda}

\footnotesize{yadda}

\small{yadda}

\normalsize{normal yadda}

:::
::: column

\large{yadda}

\Large{yadda}

\LARGE{yadda}

\huge{yadda}

\Huge{yadda}

:::
::::
