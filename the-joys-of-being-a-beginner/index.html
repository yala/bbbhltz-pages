<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="/favicon.ico" />
    <title>The Joys of Being a Beginner | Digressional Didact</title>
    <meta name="description" content="I could do this all day" />
    

    <link rel="stylesheet" href="https://cdn.simplecss.org/simple.min.css">
    <link rel="stylesheet" href="/css/admonitions.css">
</head>
<body>
    <header>
  <h1>The Joys of Being a Beginner</h1>
  <p>I could do this all day</p>
  <nav>
  <a href="/" title="">Home</a><a href="/blog" title="">Blog</a><a href="/tags" title="">Tags</a><a href="/about" title="">About</a><a href="/contact" title="">Contact</a><a href="/blogroll" title="">Blogroll</a>
</nav>
</header>
<main>
<nav align="center">
    
        
            <a href="https://bbbhltz.codeberg.page/tags/programming/">#programming</a>
            <a href="https://bbbhltz.codeberg.page/tags/python/">#python</a></nav>


<p>
<b>Posted</b>: 22 February 2022
<br><b>Stats</b>: 1011 words / ~5  minutes </p>

<p>Have you ever player <em><a href="https://en.wikipedia.org/wiki/Stardew_Valley">Stardew Valley</a></em>? It is a sort of idle game, an improved version of <em><a href="https://en.wikipedia.org/wiki/Harvest_Moon_(2007_video_game_series)">Harvest Moon</a></em>, about farming and fishing, among other things. When you first play the game, you want nothing more than to advance. You want to have a bigger house, a horse, visit the areas that you couldn&rsquo;t at the start of the game. The further you progress, the more there is to do. Very quickly, you will find yourself rushing around without an idle moment. You will wake up, check calendars and weather reports, tend to the garden, go to town, go to the mines, go fishing, and realize that you need to get back home before time runs out. The fun remains, but there is also a nostalgia for that first day. To be able to roll out of bed, water a few parsnips and not really worry about the rest. This is the joy of being a new player. The joy of being a beginner.</p>
<hr>
<p>A certain number of years ago, I was a student. I absolutely enjoyed being a student. There were some classes that I just couldn&rsquo;t get my head around. One of those was beginner programming. Much like when I started playing <em>Stardew Valley</em> a few years back, I was fed up with the beginner stuff. I wanted more. When the exams came, I was not ready. I took my bad grades, and decided that programming was not something for me.</p>
<p>Reflecting on this part of my life, I am now certain the professors were doing an excellent job teaching the subject. I was just not ready to be a beginner. Western culture wants us to be <em>experts</em> and <em>proficient</em>. We need to be <em>fast learners</em> with <em>high uptake bandwidth</em> with innate logic and <em>critical thinking</em> skills. Just being <em>literate</em> is not enough. Would you even bother putting a skill on your CV/résumé if you were only a <em>novice</em> at that task?</p>
<p>Since those days, much of which I have now concluded were focused on <em>learning to learn</em>, I have, in fact, become slightly more proficient at learning. It is a recent development, though, as far as I can tell. Perhaps it was learning little things related to computers, or living in another country, or meeting new people, or having different jobs with different tasks. Whatever the reason, whenever it happened, the joys of being a beginner have revealed themselves to me.</p>
<p>Plenty of pompous people produce pithy posts parleying the pros of practising self-improvement and the perfectionment of one&rsquo;s cognitive processes. <em>Learning is life, life is learning</em>, yadda yadda yadda. Rarely do they stop to remind us that, when we were children, we enjoyed learning something new. Learning to ride a bike, learning to play badminton, learning to swim. Toddlers look like they are having fun just learning to walk! But, those same toddlers, once they become proficient, find themselves bored with the activity and demand to be carried.</p>
<p>If I were to describe my current skill level in programming, I would likely use the term <em>sub-toddler</em>. I would say it with a large smile. The last time I have had this much fun was when I was first learning about Linux, but this is more akin to when I was learning French at age 12. Each day, there is a new thing, and it is great. It is learning to walk, and water parsnips.</p>
<p>Take for example what I learned this morning:</p>
<p>
  <figure>
    <img src="../images/turtle.gif" alt="Turtle/Frogger">
    <figcaption>Frogger in Python/Turtle</figcaption>
  </figure>

</p>
<p>That is a <em>game</em> made using Python with the Turtle module. I don&rsquo;t think I need to tell many of you, but just in case, this is the description of Turtle:</p>
<blockquote>
<p>Turtle graphics is a popular way for introducing programming to kids. It was part of the original Logo programming language developed by Wally Feurzeig, Seymour Papert and Cynthia Solomon in 1967. <sup id="fnref:1"><a href="#fn:1" class="footnote-ref" role="doc-noteref">1</a></sup></p>
</blockquote>
<p>Notice that is something meant for <em>introducing programming to kids</em>. I am not a <em>kid</em>, and <em>kid</em> is not even considered academic English where I teach. When I saw the exercise I was meant to work on, I thought to myself, &ldquo;really?&rdquo; How will this teach me anything? I have been slowly plodding along learning some Python basics, and now I am drawing shapes like I use to on my calculator in high school?</p>
<p>It only took me 10 minutes to realize that <mark><strong>I could do this all day</strong></mark>. It is akin to learning how to use a <a href="https://en.wikipedia.org/wiki/Bescherelle">Bescherelle</a>. It is dry, it is not intrinsically <em>fun</em>, you would never brag about it, but it gives you some fundamental skills that allow you to progress. Most importantly, though, it is <em>novel</em> when you have no skill whatsoever. This is one of the most captivating things I have done with a computer since getting a Raspberry Pi, or flashing a new ROM to my phone. The steps are few. The requirements are low. But, there is a little voice asking if it will work; when I click the button, a moment of anxiety as it starts to load; and a spark of joy upon seeing shapes and colours on the screen.</p>
<p>Of course, I won&rsquo;t be able to dork around with Turtle forever. In a few weeks, I will have likely forgotten how I managed to solve the problem and get things to work per the instructions, but I will not soon forget how <em>wonderful</em> it is to spend a <em>little</em> more time than necessary on the beginner exercises.</p>
<p>Would it be cliché to end with a call to action to <em>never stop learning</em>? To say <em>learning is fun</em>? What about a little, <em>if I can do this, so can you</em>?</p>
<p>Yes, yes, and yes.</p>
<p>It should, however, go without saying that being a <em>beginner</em> is not to be frowned upon. In a few weeks &mdash; if I continue down my path of trying to expand my skills, and these beginner exercises get further behind me &mdash; I suspect the quale of the experience shall remain present.</p>
<p>Now, back to drawing shapes.</p>
<section class="footnotes" role="doc-endnotes">
<hr>
<ol>
<li id="fn:1" role="doc-endnote">
<p><a href="https://docs.python.org/3/library/turtle.html">turtle — Turtle graphics — Python 3.10.2 documentation</a>&#160;<a href="#fnref:1" class="footnote-backref" role="doc-backlink">&#x21a9;&#xfe0e;</a></p>
</li>
</ol>
</section>


</main>
    <footer>
<a href="/rss.xml">RSS</a> / <a rel="me" href="https://fosstodon.org/@bbbhltz">Fosstodon</a> / <a href="https://codeberg.org/bbbhltz/pages">Source</a><br>

<a href="https://creativecommons.org/licenses/by-sa/4.0/">CC BY-SA 4.0</a><br>

<a href="https://512kb.club"><img src="https://512kb.club/assets/images/green-team.svg" style="height:1.5em;"></a><br>

Surf the <a href="https://fediring.net/">Fediring</a>: <a href="https://fediring.net/previous?host=bbbhltz.codeberg.page">Previous</a> /  <a href="https://fediring.net/next?host=bbbhltz.codeberg.page">Next</a><p>

<a href="https://codeberg.org/bbbhltz" rel="me"></a>
<a href="https://framagit.org/bbbhltz" rel="me"></a>
<a href="https://github.com/pasdechance" rel="me"></a>

</footer>
</body>
</html>
