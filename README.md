# bbbhltz.codeberg.page source



## Credits for this site

### Tools

- **Hugo** ([https://gohugo.io/](https://gohugo.io/)): *an open-source static site generator*.
- **Simple.css** ([https://simplecss.org/](https://simplecss.org/)): *framework to make a good-looking site quickly*.

### Tutorials

* **mogwai's tutorial for applying simple.css to Hugo** ([https://mogwai.be/creating-a-simple.css-site-with-hugo/](https://mogwai.be/creating-a-simple.css-site-with-hugo/))

### Hosting

* This blog was originally self-hosted, then moved to **sourcehut pages** ([https://srht.site/](https://srht.site/)) and is now on **Codeberg Pages** ([https://codeberg.page/](https://codeberg.page/)).
