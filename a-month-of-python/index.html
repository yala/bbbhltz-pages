<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="/favicon.ico" />
    <title>A Month of Python | Digressional Didact</title>
    <meta name="description" content="Learning something new, a handful of hours each week" />
    

    <link rel="stylesheet" href="https://cdn.simplecss.org/simple.min.css">
    <link rel="stylesheet" href="/css/admonitions.css">
</head>
<body>
    <header>
  <h1>A Month of Python</h1>
  <p>Learning something new, a handful of hours each week</p>
  <nav>
  <a href="/" title="">Home</a><a href="/blog" title="">Blog</a><a href="/tags" title="">Tags</a><a href="/about" title="">About</a><a href="/contact" title="">Contact</a><a href="/blogroll" title="">Blogroll</a>
</nav>
</header>
<main>
<nav align="center">
    
        
            <a href="https://bbbhltz.codeberg.page/tags/technology/">#technology</a>
            <a href="https://bbbhltz.codeberg.page/tags/education/">#education</a>
            <a href="https://bbbhltz.codeberg.page/tags/python/">#python</a>
            <a href="https://bbbhltz.codeberg.page/tags/programming/">#programming</a></nav>


<p>
<b>Posted</b>: 22 January 2022
<br><b>Stats</b>: 1485 words / ~7  minutes </p>

<p><em>A month ago today, I <a href="https://fosstodon.org/web/@bbbhltz/107487475756207610">began pondering</a> what to do with my time off. My schedule from now until the end of the school year has provided me with a day off. In contrast to previous years, when I would have gone out and found another employer, I wanted to learn something.</em></p>
<h2 id="teaching-the-teacher">Teaching the Teacher</h2>
<p>My employer provides us with training. We just have to ask for it and convince our manager, and it is approved (most of the time, if it isn&rsquo;t too ridiculous). Personally, I have nothing against learning something new. I was &ldquo;school smart&rdquo;, and I really enjoyed my studies. On more than one occasion, I have signed up for training. Three times, in fact. Three times I took the same course: Spanish.</p>
<p>The first year started out fine, and I felt some progress, until my schedule changed, and I was unable to continue attending the class. The next year, the same thing. The following year, I took a Spanish course online. It was a disaster. A total joke. Many language courses start off with a test to determine your level, and very good online training software uses computerized adaptive testing to continuously evaluate the learner. The online course I took did not have this option, and the initial test was a multiple choice test about colours and numbers. It was determined that I have an excellent level of Spanish, which is not the case. I completed the 20 hours. It was confusing and difficult, and I learned nothing that I can use in daily conversation.</p>
<p>Learning Spanish is on the back burner for now.</p>
<p>I dove deep into the available options. I had several options:</p>
<ul>
<li>Take a course provided by my employer</li>
<li>Take a course available through the &ldquo;<a href="https://fr.wikipedia.org/wiki/Compte_personnel_de_formation">Compte personnel de formation</a>&rdquo; (a French financing scheme for training)</li>
<li>Take a course and pay for it myself</li>
<li>Learn by myself</li>
</ul>
<p>Taking courses through my employer is an exceptional idea, but I missed a deadline. I began preparing a shortlist on my CPF. There are too many courses to even think about, but I managed to find quite a few that met my criteria (hours, cost). I searched for courses that were not available through the first two options, but found it hard to determine which training courses were reputable, worth my money, or total scams.</p>
<p>I hesitated even thinking about learning my myself. As mentioned above, I was &ldquo;school smart&rdquo;, but the context is different now. I am not a student, I don&rsquo;t have deadlines or homework. Would I even bother? Hard to say.</p>
<p>Then, while doing my research and preparing my lists and weighing my choices, something dawned on me. I liked being a student, and I need that regular (weekly) dose of learning to maintain a sense of progress. I know from my job that it takes more than an hour or two per week to progress. Finally, learning something new by myself is not that crazy. It is what teachers and professors do regularly when given a new subject to teach.</p>
<p>So, that is what I have been doing the past three weeks.</p>
<p>I am trying to learn Python.</p>
<p>How? By preparing a lesson plan, creating my own exercises (so I feel less inclined to give up and just go look at the answers), and <a href="https://codeberg.org/bbbhltz/learningpython">presenting it as a course that I would teach</a>.</p>
<p>Yes, I am aware that the <em>beginner teaching beginner</em> thing is very cliché. I cannot argue with results, though.</p>
<h2 id="a-month-of-python">A Month of Python</h2>
<p>
  <figure>
    <img src="../images/LearningPython.png" alt="Screenshot: https://codeberg.org/bbbhltz/learningpython">
    <figcaption>Screenshot of https://codeberg.org/bbbhltz/learningpython</figcaption>
  </figure>

</p>
<p>These past few weeks have been very enjoyable. I have not enjoyed sitting in front of a screen this much since I first installed Linux. I have been making slow, but steady, progress.</p>
<p>The last time I tried programming was over 15 years ago at university. It was Java. This is a summary of how it went:</p>
<ul>
<li>Excellent in-class experience with great professors</li>
<li>Great labs with good projects and fun TAs</li>
<li>Challenging, but not impossible, homework assignments</li>
<li><strong>Exams that made me think I sat down at the wrong table!</strong></li>
</ul>
<p>I still remember leaving an exam, going back to my dorm room, and looking through my notes. The last lesson was easy. So, <em>how</em> did we get from &ldquo;Make an input box that asks for your birthday and prints your age&rdquo; to &ldquo;Make Tetris&rdquo;? A mystery for sure, and a very low grade. Second semester, it was the same. The exam was far more difficult than the class, but I was ready. I did not produce anything close to a program. I just wrote out everything we had learned in class, and made sure to comment it to prove I knew what I was doing, but didn&rsquo;t know how to get all those things to work together. I got a B+ and decided that programming wasn&rsquo;t for me.</p>
<p>Clearly, I have been putting this off for too long &mdash; both learning Python, and publicly keeping track of that learning.</p>
<h2 id="lesson-building">Lesson Building</h2>
<p>A favourite class when I was a student was learning about teaching. There was a lot of theory, which is what one would expect. At the time there was a lot of talk about online learning (this was around 2002), and the approach to online learning was meant to be the same as what was applied in face-to-face learning (i.e. instructional design). There are variations on these methods. The one I always found easiest to apply was <strong>ADDIE</strong> model.</p>
<p>ADDIE &mdash; analyse, design, develop, implement, evaluate &mdash; dictates that I (paraphrasing here) look at my needs first. Well, that is easy. Close to zero programming know-how, so, I start at the beginning. Then, I would need to design and develop. So, I looked at a rather nice collection of books, comparing their table of contents and proposed learning paths to see a common thread. I also looked at what MOOCs recommended, along with the previously mentioned courses provided by my employer and other services.</p>
<p>I didn&rsquo;t follow the model to the letter here. I did not sit down like I would when preparing my real lessons. I did no storyboarding. I prepared no definitive bibliography. A lot is based on the work of others.</p>
<p>Currently, I am in the implementation phase: doing the actual work. I have had to backtrack a few times due to unfamiliar concepts that required me to review fundamentals.</p>
<p>My personal learning path can be broken down as follows:</p>
<ul>
<li>Pre-requisites (not actual Python, but computer science notions that I stop and look into)</li>
<li>Fundamentals (basics, but the easiest of basics)</li>
<li>Basics (where things become more challenging, still in progress)</li>
<li>Intermediate</li>
<li>Advanced</li>
</ul>
<p>While I know what I will be doing in the latter sections, I don&rsquo;t know how I will feel about it. I do need to go back and review the previous sections and think of different exercises to test myself.</p>
<h2 id="early-success">Early Success</h2>
<p>Without even getting beyond the beginner chapters, there are already a few things of which I am particularly proud. They are not beautiful, colourful, or elegant. They work, and they made me think.</p>
<h3 id="the-witch-the-what">The Witch (the what?!)</h3>
<p>If you have ever played the <a href="https://www.barnonedrinks.com/games/w/witch-410.html">Witch Drinking Game</a>, you know what this is about. If you haven&rsquo;t, it doesn&rsquo;t matter. The game is not as hard as it appears.</p>
<p>What I enjoy about this game is not the drinking. It is an example that I use in class with my students. I have them do this game as an icebreaker. What happens when I describe it is confusion. My students are not native speakers, so they initially think they misunderstood something. Then, when they play, it all makes sense.</p>
<p>Learning Python is analogous to this, for me at least. I read, I try, I doubt my understanding, I try again, and things turn out fine.</p>
<p>So, <a href="https://codeberg.org/bbbhltz/learningpython/src/branch/main/content/2.2-Basics2.md#practice-13">I gave myself a challenge</a>. It is just a script that generates a dialogue, and I found a solution that works. It is very motivating. Initially, I did not think I understood, or would be able to do it. Then it started working. At the end, I smiled and laughed. Much like my students. A tiny success; an immeasurable win.</p>
<h2 id="month-two">Month Two</h2>
<p>As I continue learning, I continue having new ideas. And, accordingly, the idea that I have requires what I planned on working on next. I like the challenge, and what it highlights: despite being relatively good with computers, there are notions that are foreign to me.</p>
<p>The goal of learning this is coming into view. If I continue down this path, I will be able to put this tool to work at my job and automate a few tasks. Alternatively, I can show what I have learned to my managers, and they may allow me to develop a course that I can teach. Who knows? Personally, and professionally, I can see how learning one of the most popular programming languages will help me in the future.</p>


</main>
    <footer>
<a href="/rss.xml">RSS</a> / <a rel="me" href="https://fosstodon.org/@bbbhltz">Fosstodon</a> / <a href="https://codeberg.org/bbbhltz/pages">Source</a><br>

<a href="https://creativecommons.org/licenses/by-sa/4.0/">CC BY-SA 4.0</a><br>

<a href="https://512kb.club"><img src="https://512kb.club/assets/images/green-team.svg" style="height:1.5em;"></a><br>

Surf the <a href="https://fediring.net/">Fediring</a>: <a href="https://fediring.net/previous?host=bbbhltz.codeberg.page">Previous</a> /  <a href="https://fediring.net/next?host=bbbhltz.codeberg.page">Next</a><p>

<a href="https://codeberg.org/bbbhltz" rel="me"></a>
<a href="https://framagit.org/bbbhltz" rel="me"></a>
<a href="https://github.com/pasdechance" rel="me"></a>

</footer>
</body>
</html>
