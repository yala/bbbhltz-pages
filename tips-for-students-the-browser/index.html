<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="/favicon.ico" />
    <title>Tips for Students: The Browser | Digressional Didact</title>
    <meta name="description" content="Hint: don&#39;t open 100 tabs." />
    

    <link rel="stylesheet" href="https://cdn.simplecss.org/simple.min.css">
    <link rel="stylesheet" href="/css/admonitions.css">
</head>
<body>
    <header>
  <h1>Tips for Students: The Browser</h1>
  <p>Hint: don&#39;t open 100 tabs.</p>
  <nav>
  <a href="/" title="">Home</a><a href="/blog" title="">Blog</a><a href="/tags" title="">Tags</a><a href="/about" title="">About</a><a href="/contact" title="">Contact</a><a href="/blogroll" title="">Blogroll</a>
</nav>
</header>
<main>
<nav align="center">
    
        
            <a href="https://bbbhltz.codeberg.page/tags/education/">#education</a>
            <a href="https://bbbhltz.codeberg.page/tags/tips/">#tips</a></nav>


<p>
<b>Posted</b>: 28 January 2021
<br><b>Stats</b>: 1539 words / ~8  minutes </p>

<p><strong>Hello Internets. This is a guide for the average student, particularly students in the first year of post-secondary education. If you are a power user or generally geeky person, nothing here will be of particular interest to you.</strong></p>
<h2 id="0-introduction">0. Introduction</h2>
<p>Good day all,</p>
<p>My name is at the top of this blog, but that does not tell you who I am. I am a professor at a business school in France. Over the course of 14 odd years of teaching I have become frustrated with how my students use their computers. This series of blog posts (there will be at least six) will address those frustrations.</p>
<p>So, what frustrates me? Not that much, in fact. It varies from the rather basic things like having loads of icons on your desktop to things that should be straightforward, like how to double-space a Word document and take a screenshot (no, it is not taking a picture of your computer with your phone). I have had several students that don&rsquo;t know how to do either.</p>
<p>Being a person born in the 80s I have been using computer for roughly 30 years. I experienced the glorious arrival of the web and Wi-Fi and watched as computers became mainstream for students in post-secondary education. In France, this was in contrast to what I had experienced in Canada where most, if not every student, used a computer or laptop while at university (even before the arrival of Wi-Fi and batteries that lasted more than a single lesson).</p>
<p>This experience left me with very general knowledge that I always considered common knowledge, but now I have committed myself to blogging a little and I will share with you at least six sets of recommendations:</p>
<ul>
<li>using your browser</li>
<li>using your word processor for report writing (<a href="https://bbbhltz.space/posts/miniguide-report-writing/">link</a>)</li>
<li>making presentations (WIP)</li>
<li>taking notes and being organized (WIP)</li>
<li>using the keyboard (WIP)</li>
<li>recommended software and other sundries (WIP)</li>
</ul>
<p>Any software used will be free software, but the recommendations and tips can be applied to proprietary software used on Windows, Mac or Linux.</p>
<h2 id="1-the-browser">1. The Browser</h2>
<h3 id="general-rules">General Rules</h3>
<p>As a student there are a few things we should keep in mind. In order to have a nice workflow I have always been a <strong>SOUP</strong> enthusiast.</p>
<p>What is <strong>SOUP</strong>?</p>
<p><strong>S</strong>imple: Giant, resource-heavy, complicated software will slow things down.
<strong>O</strong>ne App; One Purpose: You shouldn&rsquo;t need several browsers installed, the apps you use should be good at doing that one thing you need them to do.
<strong>U</strong>p-to-date: You should keep all of your software, and operating system, as up-to-date as possible.
<strong>P</strong>ay it forward: You should show your friends how to do things when possible, otherwise you will become the &ldquo;PowerPoint Guy&rdquo; or the &ldquo;Premiere Pro Girl&rdquo;.</p>
<h3 id="tabs-and-toolbars">Tabs and Toolbars</h3>
<p>If your browser is open right now, at the top of your screen you will see a title bar, an address bar, maybe a bookmarks bar, and a list of open tabs. I don&rsquo;t want to be too annoying or pedantic, but when I look at a student&rsquo;s screen and see just how many tabs they have open and hear them complaining about how their computer is slow or their connection is bad, I want to grab them and shake them vigorously.</p>
<p>Of course things are slow! Your browser has over 20 tabs open, and it looks like at least four of them are Facebook.com, I see a Messenger.com, LinkedIn, two Zoom tabs for some reason, Teams, Outlook, Gmail, a Google Doc, and many Google searches, AND Netflix!</p>
<p>I am not an expert, and there are things that I cannot always explain clearly despite being a professor. I have, time and again, tried to get this message across. Yes, your browser can open multiple tabs, but each tab is like opening a new browser. You have several browsers running inside your browser. Each one uses some memory. Websites today require more processing power and memory than some apps.</p>
<p>Let&rsquo;s look at some numbers.</p>
<p><strong>These screenshots come from a Raspberry Pi which is <em>not</em> a powerful computer</strong></p>
<p>Here is the Task Manager of my browser with one tab open:</p>
<p>
  <figure>
    <img src="https://i.imgur.com/rH4mYnC.png" alt="Firefox Task Manager, not doing much">
    <figcaption>Idle Task Manager</figcaption>
  </figure>

</p>
<p>The Task Manager and the New Tab use some memory. All of those puzzle piece are extensions, and they eat up a little memory for each tab too.</p>
<p>If we look at my system Task Manager we can see that the browser is using a certain percentage of CPU but also memory (RSS is &ldquo;the portion of memory occupied by a process that is held in main memory (RAM)&rdquo; but we don&rsquo;t need to know that because I&rsquo;m going to make those numbers go up a bit later). The extensions, too, are using some memory.</p>
<p>
  <figure>
    <img src="https://i.imgur.com/cDfiTfF.png" alt="Firefox using some memory">
    <figcaption>Firefox Running</figcaption>
  </figure>

</p>
<p>So, that is a browser doing nothing. Let&rsquo;s do some stuff, OK? I think I need to open my personal email, my work email, Teams, and why not IMGUR too. Strangely enough, my weak little Pi doesn&rsquo;t even show one of the tabs that are open, but Teams and Outlook use between 5 and 10x the memory of a page like IMGUR.</p>
<p>
  <figure>
    <img src="https://i.imgur.com/n9TIx2r.png" alt="Alright little Pi, I think you can, I think you can">
    <figcaption>Memory Usage</figcaption>
  </figure>

</p>
<p>And system-wide you are starting to use up RAM.</p>
<p>
  <figure>
    <img src="https://i.imgur.com/ORV5LDS.png" alt="RAM-a-lam-a-ding-dong">
    <figcaption>System-wide RAM Usage</figcaption>
  </figure>

</p>
<p>Well, you are clearly thinking the obvious. This is what computers are meant to do. So, why not, right? I certainly agree with you. Your fairly recent, probably expensive, laptop computer should be able to have many tabs open, but when things start slowing down, it is likely your fault and not because your computer is dying. Let&rsquo;s hit up Facebook and see how much memory we can use. YouTube can come along for the ride. Screw it, Google Drive and a Google search for &ldquo;chocolate chip cookies&rdquo;.</p>
<p>I will be naughty and launch TWO instances of Firefox with TWO tabs each.</p>
<p>
  <figure>
    <img src="https://i.imgur.com/jQ1iNqu.png" alt="You get the idea">
    <figcaption>Multiple instances</figcaption>
  </figure>

</p>
<p>I think you get it. Numbers go up. Your RAM is not infinite. Combine this with perhaps Zoom or even Adobe Acrobat and things start to slow down. You can close some tabs.</p>
<h3 id="bookmarks">But, then I&rsquo;ll lose my place&hellip;</h3>
<p>Yes, you will, but there is a super feature in your browser called <strong>bookmarking</strong>.</p>
<p>The main browsers that people generally use are Chrome, Firefox, and Safari. They all have cousins whose names are Brave, Vivaldi, Chromium, and Opera. Those browsers have bookmark managers. So, bookmark the hell out of the pages you want to keep. You can even make folders! That way things stay organized. Those bookmarks can sync across devices. Now and then, sit down and Marie Kondo your bookmarks. Delete the just, organize the other stuff.</p>
<p>Speaking of bookmarks, you might have a bookmark bar at the top of your browser. This is a nice place to put the links you use daily. One thing that lots of people do, but a few people do not, is set just the icon on their bookmark bar. It is easy. Bookmark a site, set it to the &ldquo;Bookmarks Toolbar&rdquo; or whatever it is called on your browser, and delete the name. Done. Just an icon. Yay.</p>
<p>
  <figure>
    <img src="https://i.imgur.com/cCFc0d9.png" alt="Purty">
    <figcaption>This sparks joy</figcaption>
  </figure>

</p>
<p>You can also put folders on that bar. For students, I have seen some good ideas. Some students have a folder for each day of the week. Others for each class. Some students have their personal folder, a school folder, and a work folder, and others have the important links as icons on the toolbar, and a folder for &ldquo;fun&rdquo;.</p>
<p>Finally, we get to extensions. I will have a post later on about some recommended extensions that I use. Right now, you just need to apply the same rules to extensions as you do tabs on your browser and apps on your phone. They take up some space and use memory. Not all extensions are good. Some extensions are useful but use a lot of memory. You are allowed to uninstall or disable them. Do not just keep them there out of complacency and laziness.</p>
<p>There are only two extensions that you should have: an adblocker and a password manager. You should never, ever, under any circumstances use the built-in browser password manager. Especially on a computer that you might leave on a table in a library. It is too easy to get passwords. Chrome, if I recall correctly, even has a password export feature. And you can access the passwords within the settings too. I would recommend using <a href="https://github.com/gorhill/uBlock/#installation">uBlock Origin</a> as your adblocker, and <a href="https://bitwarden.com/">Bitwarden</a> for passwords. They are both free. uBlock uses a little less memory and is up-to-date, it also has loads of options we can discuss in another blog post. Bitwarden has desktop applications, mobile phone applications, and an extension. It can also import your Chrome and Firefox passwords.</p>
<h3 id="conclusion">Conclusion</h3>
<p>Keep it <strong>SIMPLE</strong>: You don&rsquo;t need lots of tabs or lots of extensions.
Use <strong>ONE</strong> browser: You can uninstall the others. Unless it is Edge. I think you are stuck with that, but I haven&rsquo;t checked in a long time.
<strong>UPDATE</strong> your browser: This is good for security. You can also update the extensions.
<strong>PAY IT FORWARD</strong>: Give your friends advice when you can.</p>
<p>I hope somewhere, some student finds this useful. If you did, be sure to bookmark the blog. The next instalment is about writing documents and word processors.</p>
<p>Take care.</p>


</main>
    <footer>
<a href="/rss.xml">RSS</a> / <a rel="me" href="https://fosstodon.org/@bbbhltz">Fosstodon</a> / <a href="https://codeberg.org/bbbhltz/pages">Source</a><br>

<a href="https://creativecommons.org/licenses/by-sa/4.0/">CC BY-SA 4.0</a><br>

<a href="https://512kb.club"><img src="https://512kb.club/assets/images/green-team.svg" style="height:1.5em;"></a><br>

Surf the <a href="https://fediring.net/">Fediring</a>: <a href="https://fediring.net/previous?host=bbbhltz.codeberg.page">Previous</a> /  <a href="https://fediring.net/next?host=bbbhltz.codeberg.page">Next</a><p>

<a href="https://codeberg.org/bbbhltz" rel="me"></a>
<a href="https://framagit.org/bbbhltz" rel="me"></a>
<a href="https://github.com/pasdechance" rel="me"></a>

</footer>
</body>
</html>
